/*
    Wall Clock script Made by Kurtis Anatine (Vargink)
    Get in contact with me via https://keybase.io/theguywho 
    If they can't back with a hash then you better dash!

    Feel free to fork on BitBucket and get the latest! https://bitbucket.org/thecollectivesl/wall-clock-kit 
    
    This work is licenced to the public under the Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/ formally known as AYDMD
    by myself. This works comes with no warranty, feel free to take this and make awesome things with it! (please to send me pics!)
*/
(function() {
    var self = {};
    self.debug = false;
    function debug(message) {
        if (self.debug) {
            print(message);
        }
    }
    this.preload = function(entityID) {
        self.entityID = entityID;
        self.overlayID = Overlays.addOverlay('model', {
            visible: true,
            url: Script.resolvePath("../objects/ClockArm.fbx"),
            parentID: self.entityID,
            position: Entities.getEntityProperties(self.entityID).position,
            rotation: Entities.getEntityProperties(self.entityID).rotation,
            dimensions: Entities.getEntityProperties(self.entityID).dimensions
        });
        self.intervalID = Script.setInterval(function() {
            var jointNames = Overlays.getProperty(self.overlayID, 'jointNames');
            debug("Joints Length: "+jointNames.length);
            if (jointNames.length !== 0) {
                Script.clearInterval(self.intervalID);
                for (var i = 0; i < jointNames.length; i++) {
                    switch (jointNames[i]) {
                        case "Hour":
                            self.indexHour = i;
                            break;
                        case "Minute":
                            self.indexMinute = i;
                            break;
                        case "Second":
                            self.indexSecond = i;
                            break;
                    }
                }
                debug("indexHour: "+self.indexHour+" indexMinute: "+self.indexMinute+" indexSecond: "+self.indexSecond);
                self.intervalID = Script.setInterval(function() {
                    var d = new Date();
                    var clockHands = JSON.parse(Entities.getEntityProperties(entityID).userData);
                    var newRotation = Overlays.getProperty(self.overlayID, 'jointRotations');
                    newRotation[self.indexHour] = Quat.fromPitchYawRollDegrees(90, ((d.getUTCHours() + clockHands.houroffset) * 30) + (d.getUTCMinutes() * 0.5), 0);
                    newRotation[self.indexMinute] = Quat.fromPitchYawRollDegrees(90, d.getUTCMinutes() * 6, 0);
                    newRotation[self.indexSecond] = Quat.fromPitchYawRollDegrees(90, d.getUTCSeconds() * 6, 0);
                    Overlays.editOverlay(self.overlayID, {
                        "jointRotations": newRotation,
                        "dimensions": Entities.getEntityProperties(self.entityID).dimensions
                    });
                }, 1000);
            }
        }, 3000);
    };
    this.unload = function() {
        Overlays.deleteOverlay(self.overlayID);
        Script.clearInterval(self.intervalID);
    };
});
