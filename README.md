[Version]:1
[Grid]:button
[end]:end
![Wall Clock Banner](https://bitbucket.org/thecollectivesl/wall-clock-kit/raw/master/img/WallClockKit.png)
# The Wall Clock Kit #

This is a clock model and a script which tells the time. Add it to High Fidelity with [this link](https://bitbucket.org/thecollectivesl/wall-clock-kit/raw/master/wallclock.json)

### Features ###

* Change the clock face and back easily within High Fidelity
* Adjust the time for your timezone easily without editing the script.

### Change Clock Face and Back ###

The two faces of the clock are ClockFace and ClockBack so you can change the face of the clock by changing these textures. You can get the shading and an alpha for the [ClockFace](https://bitbucket.org/thecollectivesl/wall-clock-kit/src/master/objects/textures/clockface/) and the [ClockBack](https://bitbucket.org/thecollectivesl/wall-clock-kit/src/master/objects/textures/clockback/) in these links provided. You can also get templates for Gimp [here](https://bitbucket.org/thecollectivesl/wall-clock-kit/src/master/objects/textures/). You can change these by placing either one or the other of these values in the textures area in the clocks properties. 
Here are some examples for applying textures.

Blue background and the high fidelity logo.
```
{
    "ClockBack": "https://bytebucket.org/thecollectivesl/wall-clock-kit/raw/master/objects/textures/clockback/Blue.png",
    "ClockFace": "https://bytebucket.org/thecollectivesl/wall-clock-kit/raw/master/objects/textures/clockface/HiFi.png"
}
```
Blue background only
```
{
    "ClockBack": "https://bytebucket.org/thecollectivesl/wall-clock-kit/raw/master/objects/textures/clockback/Blue.png"
}
```
JimJamz Logo (as seen on [JimJamz!](https://www.youtube.com/watch?v=SktGXWCmCAs))
```
{
    "ClockFace": "https://bytebucket.org/thecollectivesl/wall-clock-kit/raw/master/objects/textures/clockface/JimJam.png"
}
```

### Adjust Time ###

The time can be adjusted by editing the "houroffset" value in the User Data area. By default this is set to High Fidelity time.

### Licences and Acknowledgement ###

This work is licenced to the public under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) formally known as AYDMD by myself. This works comes with no warranty, feel free to take this and make awesome things with it! (please to send me pics!)

High Fidelity logo is created and owned by [High Fidelity](https://highfidelity.io)
JimJamz logo is created and owned by [High Fidelity](https://highfidelity.io)
Clouds Sky Background Comic is created by [Alexandra_Koch](https://pixabay.com/en/clouds-sky-background-comic-2390892/)

### Who do I talk to? ###

* [Kurtis Anatine](https://keybase.io/theguywho)